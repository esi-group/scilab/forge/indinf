// ====================================================================
// Copyright (C) 2012 - Jorge Catumba Ruiz
// 
// This file must be used under the terms of the GPLv3.
// This source file is licensed as described in the file license.txt, which
// you should have received as part of this distribution.
// ====================================================================

// Trying pagerank(Matrix,Index,'limit')

A=[
0 2 1 0
3 0 1 4
0 0 0 0
2 4 1 0
];
expd=[
0.1718456
0.4366608
0.0375  
0.3539936
];
d=pagerank(A,0.85,'limit');
assert_checkalmostequal(d,expd,0,1D-5,"element");

// Trying pagerank(Matrix,Index,'matlab')

expd=[
0.1718456
0.4366608
0.0375  
0.3539936
];
d=pagerank(A,0.85,'matlab');
assert_checkalmostequal(d,expd,0,1D-5,"element");

// Trying pagerank(Matrix,Index,'power')

expd=[
0.1708551
0.4391617
0.0375  
0.3524832
];
d=pagerank(A,0.85,'power');
assert_checkalmostequal(d,expd,0,1D-5,"element");

// Trying pagerank(Matrix,Index,'sparsepower')

expd=[
0.1708551
0.4391617
0.0375  
0.3524832
];
d=pagerank(A,0.85,'sparsepower');
assert_checkalmostequal(d,expd,0,1D-5,"element");
