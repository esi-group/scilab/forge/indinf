help_lang_dir = get_absolute_file_path('build_help.sce');

help_from_sci(toolbox_dir + "macros", toolbox_dir + "help/en_US");
tbx_build_help(TOOLBOX_TITLE, help_lang_dir);

clear help_lang_dir;
