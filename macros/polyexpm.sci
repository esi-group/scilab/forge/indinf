function [E] = polyexpm(D)
// Returns the exponential matrix for a polynomial matrix.
//
// Calling Sequence
//  polyexpm(D) // Show the the exponential matrix of the polynomial matrix D.
//  E=polyexpm(D) // Store the exponential matrix of the polynomial matrix D in the E variable.
//
// Parameters
// D: Polynomial matrix to calculate its exponential.
// E: Name of the variable to store the exponential matrix of a given polynomial matrix.
//
// Description
// This function calulates the exponential matrix of a polynomial matrix using the analytic definition of the exponential function of a matrix. Use with careful. This function is required by the pwp and heatkernel functions.
//
// Examples
// t=poly(0,'t')  // Define the variable t.
// D = [t 1-t;t^2 1] // Define a 2x2 matrix with polynomial entries.
// E = polyexpm(D) // Calculates the exponential matrix of the polynomial matrix D.
//
// See also
//  micmac
//  pwp_method
//  heatkernel
//
// Authors
//  Jorge Catumba ;

E=eye(size(D,"c"),size(D,"c"))
  for n=1:100
    E=E+D^n/factorial(n)
  end
endfunction
