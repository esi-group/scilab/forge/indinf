function H=part_adj_mat(M,Vlist)
// Returns the adjacency matrix of the partition graph P of a graph G using its adjacency matrix.
//
// Calling Sequence
//   part_adj_mat(M,Vlist)
//   H = part_adj_mat(M,Vlist)
//
// Parameters
//  M: Adjacency matrix of the initial graph G.
//  Vlist: A list of the desired partition blocks, this is a list of vectors where each one contains the number of the nodes for each block.
//  H: Adjacency matrix of the partition of the graph G.
//
// Description
//  This function gives the adjacency matrix of the partition graph P of a graph G using its adjacency matrix.
//
// Examples
//  M=[0 0 3 1;0 0 2 0;1 1 0 1;3 0 0 0]
//  Vlist=list([1,3],[2,4])
//  H=part_adj_mat(M,Vlist)
//
// See also
//  sum_matrix_blocks
//  micmac
//  pagerank
//  pwp_method
//  heatkernel
//
// Used functions
//  spilu_permVecToMat
//
// Authors
//  Jorge Catumba ;

    j=size(M,'c')
    vcompare=1:j
    sizelist=size(Vlist)
    sumsize=0
    uvect=[]
    permvect=[]
    realsize=[]
    difference=[]
    position=0

    for index=1:sizelist
        realsize(1,index)=size(Vlist(index),'c')
        uvect=union(uvect,Vlist(index))
        for indexj=1:realsize(index)
            position=position+1
            permvect(1,position)=Vlist(index)(indexj)
        end
    end

    sumsize=sum(realsize)
    sizepermvect=size(permvect,'c')
    sizeuvect=size(uvect,'c')

    if (sumsize>j | sizelist<=0 | sizeuvect~=sumsize | max(uvect)>j) then
        error('Incongruent partition.')
    else
        if (sizeuvect~=size(vcompare,'c')) then
            difference=setdiff(vcompare,permvect)
            for index=1:size(difference,'c')
                permvect(1,sizepermvect+index)=difference(1,index)
            end
        end
         permmat=spilu_permVecToMat(permvect)
         permutedmatrix=permmat'*M*permmat
         newsize=size(Vlist)+size(difference,'c')
         while sum(realsize)<=j
             if (sum(realsize)~=j) then
                 sizerealsize=size(realsize,'c')
                 realsize(1,sizerealsize+1)=1
             else
                 break
             end
         end

         H=sum_matrix_blocks(permutedmatrix,realsize)
         for index=1:newsize
             H(index,index)=0
         end
    end
endfunction
