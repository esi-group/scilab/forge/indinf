function D=polylogm(E)
// Returns the logarithm matrix for a polynomial matrix.
//
// Calling Sequence
//  polylogm(D) // Show the the logarithm matrix of the polynomial matrix D.
//  E=polylogm(D) // Store the logarithm matrix of the polynomial matrix D in the E variable.
//
// Parameters
// D: Polynomial matrix to calculate its logarithm.
// E: Name of the variable to store the logarithm matrix of a given polynomial matrix.
//
// Description
// This function calulates the logarithm matrix of a polynomial matrix using the analytic definition of the logarithm function of a matrix. Use with careful. This function is under development.
//
// Examples
// t=poly(0,'t')  // Define the variable t.
// D = [t 1-t;t^2 1] // Define a 2x2 matrix with polynomial entries.
// E = polylogm(D) // Calculates the logarithm matrix of the polynomial matrix D.
//
// See also
//  micmac
//  pwp_method
//  heatkernel
//
// Authors
//  Jorge Catumba ;

D=zeros(size(E,"c"),size(E,"c"))
  for n=1:100
    D=D+((-1)^(n+1)/n)*(E-eye(size(E,"c"),size(E,"c")))^n
  end
endfunction
